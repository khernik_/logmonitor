<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service handling operations on files
 *
 * Class FileService
 * @package AppBundle\Service
 */
class FileService
{

    /**
     * @var string Path to file
     */
    private $_path = '';

    /**
     * @var string Path to configuration file
     */
    private $_configuration = '';

    /**
     * Dependency injection
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this -> _configuration = $container -> getParameter('logs');
    }

    /**
     * Initialize file to work on
     *
     * @param string $path
     * @return $this
     */
    public function init($path)
    {
        $this -> _path = $path;

        return $this;
    }

    /**
     * Get array of lines in a file
     *
     * @return array
     */
    public function getFromFile()
    {
        $output = array();
        $handle = fopen($this -> _path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $output[] = $line;
            }
        }
        fclose($handle);

        return $output;
    }

    /**
     * Get FIRST line from a file based on a given attribute
     *
     * @param string $name
     * @param string $value
     * @return string
     */
    public function getLineByAttribute($name, $value)
    {
        $lines = $this -> getFromFile();

        $output = array();
        foreach ($lines as &$line) {
            $line = json_decode($line, true);
            if ($line[$name] == $value) {
                $output = $line;
                break;
            }
        }

        return json_encode($output);
    }

    /**
     * Append new line to the end of the file
     *
     * @param string $line
     */
    public function appendLineToFile($line)
    {
        file_put_contents($this -> _path, $line . PHP_EOL, FILE_APPEND);
    }

    /**
     * Replace old file's content with the new one
     *
     * @param string $content
     */
    public function replaceFile($content)
    {
        file_put_contents($this -> _path, $content);
    }

    /**
     * Replace specified line with a new one, and add new line symbol at the end of it
     *
     * @param string $old
     * @param string $new
     * @return bool
     */
    public function editLine($old, $new)
    {
        $lines = $this -> getFromFile();

        foreach ($lines as &$line) {
            if (trim($line) == trim($old)) {
                if (! strstr($new, PHP_EOL)) {
                    $new .= PHP_EOL;
                }
                $line = $new;
            }
        }

        $this -> replaceFile($lines);

        return true;
    }

    /**
     * Remove specified line from a file
     *
     * @param string $lineToRemove
     * @return bool
     */
    public function removeLineFromFile($lineToRemove)
    {
        $lines = $this -> getFromFile();

        $newLines = array();
        foreach ($lines as $line) {
            if (trim($line) != trim($lineToRemove)) {
                $newLines[] = $line;
            }
        }
        $this -> replaceFile($newLines);

        return true;
    }

    /**
     * Get specified content of a file. Saves the position of latest reading to the external
     * file, and reads to it in the next.
     *
     * @param array $log
     * @return array
     */
    public function getFileEnding($lines, $bufferLength, $lastPosition)
    {
        $lines++;

        $handle = @fopen($this -> _path, "r");
        if (!$handle) {
            return 'Cant find or open requested file';
        }

        $output = array();

        // set file pointer to the end of the file
        fseek($handle, 0, SEEK_END);

        // get number of bytes in a file - by returning position of pointer, which is at the end
        $fileSize = ftell($handle);

        // dont want the buffer to be higher than file size
        $position = -min($bufferLength, $fileSize);

        $this -> _savePositionInConfigFile($fileSize);

        if ($lastPosition == 0) $lastPosition = $fileSize;

        $firstLine = "";

        $begin = 1;

        while ($lines > 0 || ($fileSize + $position >= $lastPosition || $lastPosition == 0 || $begin == 1)) {
            $begin = 0;
            if ($err = fseek($handle,$position,SEEK_END)) {  /* should not happen but it's better if we check it*/
                echo "Error $err: something went wrong<br/>\n";
                fclose($handle);
                return $lines;
            }

            $buffer = fread($handle, $bufferLength);

            $readLines = explode("\n", $buffer);
            $readLinesNumber = count($readLines);

            if ($firstLine != "") {
                // concatenate current last line with the piece left from the previous read
                $readLines[count($readLines) - 1] .= $firstLine;
            }

            // drop first line because it may not be complete
            $firstLine = array_shift($readLines);
            if ($readLinesNumber >= $lines && $fileSize + $position + $bufferLength == $lastPosition) { // buffer contains more lines than we want to obtain
                $shortenedReadLines = array_slice($readLines, $readLinesNumber - $lines);
                $output = array_merge($shortenedReadLines, $output);
                $lines = 0;
                break;
            } elseif (-$position >= $fileSize) { // file equals to buffer length
                $output = array_merge($firstLine, $readLines, $output);
                $lines = 0;
                break;
            } else { // file bigger than lines to read
                $output = array_merge($readLines, $output);
                $lines -= sizeof($readLines);
                $position -= $bufferLength;
                $position = max($position, -$fileSize);
            }

            if (($fileSize - $position >= $lastPosition)) {
                $lines = 0;
            }
        }

        fclose($handle);

        return $output;
    }

    /**
     * Edit last position from log file inside configuration file
     *
     * @param integer $position
     * @return bool
     */
    private function _savePositionInConfigFile($position)
    {
        $config = $this -> init($this -> _configuration) -> getLineByAttribute('pathToLog', $this -> _path);
        $old = $config;
        $config = json_decode($config, true);
        $config['lastPosition'] = $position;
        $this -> init($this -> _configuration) -> editLine($old, json_encode($config));

        return true;
    }

}
