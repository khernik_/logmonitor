<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles logs configurations
 *
 * @package AppBundle\Controller
 * @Route("/log")
 */
class LogController extends Controller
{

    /**
     * Display configurations of logs, along with actions to do on them
     *
     * @Route("/", name="displayLogs")
     */
    public function displayAction()
    {
        $logs = $this -> get('file') -> init($this -> getParameter('logs')) -> getFromFile();
        foreach ($logs as &$log) {
            $log = json_decode($log, true);
        }

        return $this -> render('AppBundle:Log:display.html.twig', array(
            'logs' => $logs
        ));
    }

    /**
     * Handles editing single log entry
     *
     * @Route("/edit", name="editLog")
     */
    public function editAction(Request $request)
    {
        $path = $request -> query -> get('path');

        $oldLine = $this -> get('file') -> init($this -> getParameter('logs')) -> getLineByAttribute('pathToLog', $path);

        if ($request -> isMethod('POST')) {
            $newLine = array(
                'pathToLog' => $request -> request -> get('path'),
                'linesToRead' => $request -> request -> get('linesToRead'),
                'bufferLength' => $request -> request -> get('bufferLength'),
                'lastPosition' => 0
            );
            $this -> get('file') -> init($this -> getParameter('logs')) -> editLine($request -> request -> get('oldData'), json_encode($newLine));

            return $this -> redirectToRoute('displayLogs');
        }

        return $this -> render('AppBundle:Log:edit.html.twig', array(
            'oldData' => json_decode($oldLine, true)
        ));
    }

    /**
     * Add new log to the existing configuration
     *
     * @Route("/add", name="addLog")
     */
    public function addAction(Request $request)
    {
        if ($request -> isMethod('POST')) {
            $path = $this -> getParameter('logs');
            $newLine = array(
                'pathToLog' => $request -> request -> get('path'),
                'linesToRead' => $request -> request -> get('linesToRead'),
                'bufferLength' => $request -> request -> get('bufferLength'),
                'lastPosition' => 0
            );
            $this -> get('file') -> init($path) -> appendLineToFile(json_encode($newLine));

            return $this -> redirectToRoute('displayLogs');
        }

        return $this -> render('AppBundle:Log:add.html.twig');
    }

    /**
     * Remove existing log from the configuration
     *
     * @Route("/delete", name="deleteLog")
     */
    public function deleteAction(Request $request)
    {
        $path = $request -> query -> get('path');

        $line = $this -> get('file') -> init($this -> getParameter('logs')) -> getLineByAttribute('pathToLog', $path);

        $this -> get('file') -> init($this -> getParameter('logs')) -> removeLineFromFile($line);

        return $this -> redirectToRoute('displayLogs');
    }

}
