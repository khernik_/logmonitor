<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Handles logs changes in present time
 *
 * Class DashboardController
 * @package AppBundle\Controller
 */
class DashboardController extends Controller
{

    /**
     * Display "logging in present time" interface. The rest is handled by front-end.
     *
     * @Route("/", name="dashboard")
     */
    public function displayAction()
    {
        $logs = $this -> get('file') -> init($this -> getParameter('logs')) -> getFromFile();
        foreach ($logs as &$log) {
            $log = json_decode($log, true);
        }

        return $this -> render('AppBundle:dashboard:display.html.twig', array(
            'logs' => $logs
        ));
    }

    /**
     * Get
     *
     * @Route("/ajaxGetLatestLogs", name="ajaxGetLatestLogs")
     */
    public function ajaxGetLatestLogsAction()
    {
        $logs = $this -> get('file') -> init($this -> getParameter('logs')) -> getFromFile();
        $mappedLogsOutput = $this -> _getMappedLogsOutput($logs);

        return new JSONResponse($mappedLogsOutput);
    }

    /**
     * Take path to the log as key, and the tail array output of the log as value
     *
     * @param array $logs
     * @return array
     */
    private function _getMappedLogsOutput($logs)
    {
        $output = array();
        foreach ($logs as $log) {
            $log = json_decode($log, true);
            $output[$log['pathToLog']] = $this -> get('file')
                                        -> init($log['pathToLog'])
                                        -> getFileEnding((int)$log['linesToRead'], (int)$log['bufferLength'], (int)$log['lastPosition'], $log['pathToLog']);
        }
        return $output;
    }

}
